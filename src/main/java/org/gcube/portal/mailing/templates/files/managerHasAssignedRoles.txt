{{GATEWAY_NAME}}
----------------------------------
Dear {{SELECTED_VRE_NAME}} Manager, this is to inform you that 
{{MANAGER_FULLNAME}} has assigned role(s) to {{USER_FULLNAME}} on {{SELECTED_VRE_NAME}} VRE:

{{NEW_ROLES}}

Further info about {{USER_FULLNAME}}:

Email: {{USER_EMAIL}}
Username: {{USER_ID}}


You received this email because you are a manager of {{SELECTED_VRE_NAME}}.
