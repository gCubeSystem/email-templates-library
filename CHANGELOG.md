
# Changelog for email template library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.5.0] - 2021-05-13

Added template for roles assignments and revokation to be sent to VRE Managers to inform them when another VRE Manager operates with roles.

## [v1.4.5-SNAPSHOT] - 2021-02-16

Updated template for downtime announcement

## [v1.4.4] - 2021-01-26

# Bug Fix

Welcome create account sign in url pointed to gateway home instead of VRE home page

## [v1.4.3] - 2020-09-21

# New Features

[#19709] Email Templates library to support a new template or invitation to join VREs

# Other

Ported to git

## [v1.4.0] - 2018-06-25

Feature #11900, Revise email templates to not be intercepted by MailScanner


## [v1.0.0] - 2017-05-11

First release 
